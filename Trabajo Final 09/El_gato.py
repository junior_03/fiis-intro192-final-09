from tkinter import *
from tkinter import messagebox
from tkinter import simpledialog

def bloquear():
    for i in range(0,9):
        lista_Botones[i].config(state="disable")

def iniciarJ():
    for i in range(0,9):
        lista_Botones[i].config(state="normal")
        lista_Botones[i].config(bg="skyblue")
        lista_Botones[i].config(text="")
        t[i] = "N"
    global nombre_Jugador1,nombre_Jugador2
    nombre_Jugador1 = simpledialog.askstring("Jugador", "Jugador 1, escribe tu nombre:")
    nombre_Jugador2 = simpledialog.askstring("Jugador", "Jugador 2, escribe tu nombre:")
    turno_Jugador1.set("Turno :" + nombre_Jugador1)

def cambiar(num):
    global turno, nombre_Jugador1, nombre_Jugador2
    if t[num]=="N" and turno==0:
        lista_Botones[num].config(text="x")
        lista_Botones[num].config(bg="white")
        t[num]="x"
        turno = 1
        turno_Jugador1.set("Turno;" + nombre_Jugador2)
    elif t[num]=="N" and turno ==1:
        lista_Botones[num].config(text="o")
        lista_Botones[num].config(bg="lightblue")
        t[num]="o"
        turno = 0
        turno_Jugador1.set("Turno;" + nombre_Jugador1)
    lista_Botones[num].config(state="disable")
    verificar()
    
def verificar():
    if (t[0]=="x" and t[1]=="x" and t[2]=="x") or (t[3]=="x" and t[4]=="x" and t[5]=="x") or (t[6]=="x" and t[7]=="x" and t[8]=="x"):
        bloquear()
        messagebox.showinfo("Ganador", "Ganaste jugador" + nombre_Jugador1)
    elif (t[0]=="x" and t[3]=="x" and t[6]=="x") or (t[1]=="x" and t[4]=="x" and t[7]=="x") or (t[2]=="x" and t[5]=="x" and t[8]=="x"):
        bloquear()
        messagebox.showinfo("Ganador", "Ganaste jugador" + nombre_Jugador1)
    elif (t[0]=="x" and t[4]=="x" and t[8]=="x") or (t[2]=="x" and t[4]=="x" and t[6]=="x"):
        bloquear()
        messagebox.showinfo("Ganador", "Ganaste jugador" + nombre_Jugador1)
    elif (t[0]=="o" and t[1]=="o" and t[2]=="o") or (t[3]=="o" and t[4]=="o" and t[5]=="o") or (t[6]=="o" and t[7]=="o" and t[8]=="o"):
        bloquear()
        messagebox.showinfo("Ganador", "Ganaste jugador" + nombre_Jugador1)
    elif (t[0]=="o" and t[3]=="o" and t[6]=="o") or (t[1]=="o" and t[4]=="o" and t[7]=="o") or (t[2]=="o" and t[5]=="o" and t[8]=="o"):
        bloquear()
        messagebox.showinfo("Ganador", "Ganaste jugador" + nombre_Jugador1)
    elif (t[0]=="o" and t[4]=="o" and t[8]=="o") or (t[2]=="o" and t[4]=="o" and t[6]=="o"):
        bloquear()
        messagebox.showinfo("Ganador", "Ganaste jugador" + nombre_Jugador1)
                                    
    
        
ventana = Tk()
ventana.geometry("400x500")
ventana.title("Juego de el Gato")
turno = 0
nombre_Jugador1 =""
nombre_Jugador2 =""
lista_Botones = []
t = [] # X O N
turno_Jugador1 = StringVar()

for i in range(0, 9):
    t.append("N")
boton0 = Button(ventana,width=9,height=3,command=lambda:cambiar(0))
lista_Botones.append(boton0)      
boton0.place(x=50,y=50)          
boton1 = Button(ventana,width=9,height=3,command=lambda:cambiar(1))
lista_Botones.append(boton1)      
boton1.place(x=150,y=50)          
boton2 = Button(ventana,width=9,height=3,command=lambda:cambiar(2))
lista_Botones.append(boton2)      
boton2.place(x=250,y=50)          
boton3 = Button(ventana,width=9,height=3,command=lambda:cambiar(3))
lista_Botones.append(boton3)      
boton3.place(x=50,y=150)          
boton4 = Button(ventana,width=9,height=3,command=lambda:cambiar(4))
lista_Botones.append(boton4)      
boton4.place(x=150,y=150)          
boton5 = Button(ventana,width=9,height=3,command=lambda:cambiar(5))
lista_Botones.append(boton5)      
boton5.place(x=250,y=150)          
boton6 = Button(ventana,width=9,height=3,command=lambda:cambiar(6))
lista_Botones.append(boton6)      
boton6.place(x=50,y=250)          
boton7 = Button(ventana,width=9,height=3,command=lambda:cambiar(7))
lista_Botones.append(boton7)      
boton7.place(x=150,y=250)          
boton8 = Button(ventana,width=9,height=3,command=lambda:cambiar(8))
lista_Botones.append(boton8)      
boton8.place(x=250,y=250)    
turnoE = Label(ventana,textvariable=turno_Jugador1).place(x=120,y=20)
iniciar = Button(ventana,bg='#012',fg='white',text='Iniciar Juego',width=15,height=3,command=iniciarJ).place(x=130,y=350)
bloquear()
ventana.mainloop()   
